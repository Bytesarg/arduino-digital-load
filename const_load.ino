#include <LiquidCrystal.h>;
//lcd(RS, Enable, D4,D5,D6,D7);
LiquidCrystal lcd(7,8,9,10,11,12);
int sensorVBatt = A0;
int sensorA = A1;
int buttonUp = 5;
int buttonDown = 6;
int pwmOUT = 3;
int statusLED = 13;
int pwmSetting;
float targetA;
float tempA;
float vBatt;
float realA;
double mAh;
boolean zero;

void setup(){
  Serial.begin(9600);
  lcd.begin(16,2);
  lcd.clear();
  pinMode(statusLED,OUTPUT);
  pinMode(pwmOUT,OUTPUT);
  pinMode(buttonDown,INPUT);
  pinMode(buttonUp,INPUT);
  vBatt = 0.00;
  realA = 0.00;
  pwmSetting = 0;
  mAh = 0;
  zero = 0;
  digitalWrite(statusLED,HIGH);
}

void loop(){
  vBatt = analogRead(sensorVBatt) * 0.0049;
  if(vBatt == 0){
    if(zero == 0){
      zero = 1;
      mAh = 0;
    }
  }
  if(zero == 1){
    if(vBatt > 0){
      targetA = 1;
      pwmSetting = 150;
      zero = 0;
    }
  }
  realA = analogRead(sensorA) * 0.0049;
  if(digitalRead(buttonUp)){
    if(targetA > 0){
      targetA = 0;
      pwmSetting = 0;
    }else{
      targetA = 1;
      pwmSetting = 150;
    }
  }
  if(digitalRead(buttonDown)){mAh = 0;}
  if(vBatt < 0.80){
    targetA = 0;
    pwmSetting = 0;
  }
  if(vBatt < 0.90){if(targetA == 0.10){targetA = 0.05;}}
  if(vBatt < 0.95){if(targetA == 0.25){targetA = 0.10;}}
  if(vBatt < 1.00){if(targetA == 0.50){targetA = 0.25;}}
  if(vBatt < 1.05){if(targetA == 1.00){targetA = 0.50;}}
  if(realA < targetA){pwmSetting++;}
  if(realA > targetA){pwmSetting--;}
  if(pwmSetting < 0){pwmSetting=0;}
  if(targetA > 0){digitalWrite(statusLED,HIGH);}else{digitalWrite(statusLED,LOW);}
  analogWrite(pwmOUT,pwmSetting);  
  lcd.setCursor(0,0);
  lcd.print(vBatt);
  lcd.print("V @ ");
  lcd.print(realA);
  lcd.print("A");
  lcd.setCursor(0,1);
  mAh = mAh + (realA / 36);
  lcd.print(round(mAh));
  lcd.print("mAh tA:");
  lcd.print(targetA);
  lcd.print("      ");
  Serial.print("Batt: ");
  Serial.print(vBatt);
  Serial.print("V TargetA: ");
  Serial.print(targetA);
  Serial.print("A RealA: ");
  Serial.print(realA);
  Serial.print("A PWM: ");
  Serial.print(pwmSetting);
  Serial.print(" mAh: ");
  Serial.print(mAh);
  Serial.println();
  delay(100);
}